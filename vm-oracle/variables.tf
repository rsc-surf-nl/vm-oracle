variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key" {}
variable "image_name" {}
variable "shape" {}
variable "memory" {}
variable "cpu" {}
variable "region" {}
variable "compartment_id" {}
variable "image_compartment_id" {}
variable "user_name" {}
variable "instance_name" {
  default = "rsc-instance"
}
variable "instance_tag" {
  default = "rsc-instance"
}
variable "availability_domain" {}
variable "fault_domain" {
  default = "FAULT-DOMAIN-1"
  type    = string
}
variable "boot_volume_size" {
  type    = number
  default = 50
}
variable "security_group_rules" {
  type = list(string)
}
variable "config_security_group_rules" {
  type    = list(string)
  default = []
}
variable "cidr" {}
variable "min_os_disk_size" {
  type    = number
  default = 20
}
variable "external_volumes" {
  default = "[]"
}
variable "external_floating_ips" {
  default = "[]"
}
variable "external_networks" {
  default = "[]"
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Compute"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "Oracle"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "VM"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
