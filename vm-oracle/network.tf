locals {
  security_group_rules = distinct(concat(
    [for item in var.security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in var.config_security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")]
  ))
  tcp_in_security_group_rules  = tolist([for each in local.security_group_rules : each if lower(split(" ", each)[0]) == "in" && lower(split(" ", each)[1]) == "tcp"])
  udp_in_security_group_rules  = tolist([for each in local.security_group_rules : each if lower(split(" ", each)[0]) == "in" && lower(split(" ", each)[1]) == "udp"])
  tcp_out_security_group_rules = tolist([for each in local.security_group_rules : each if lower(split(" ", each)[0]) == "out" && lower(split(" ", each)[1]) == "tcp"])
  udp_out_security_group_rules = tolist([for each in local.security_group_rules : each if lower(split(" ", each)[0]) == "out" && lower(split(" ", each)[1]) == "udp"])
}

resource "oci_core_vcn" "vm-vcn" {

  compartment_id = var.compartment_id
  cidr_blocks    = [var.cidr]
  count          = local.external-network != null ? 0 : 1
  freeform_tags = {
    Name               = var.instance_name,
    instance_tag       = var.instance_tag,
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}

resource "oci_core_network_security_group" "network_security_group" {
  compartment_id = var.compartment_id
  vcn_id         = local.external-network != null ? local.external-network.network_id.value : oci_core_vcn.vm-vcn[0].id

  freeform_tags = {
    Name               = var.instance_name,
    instance_tag       = var.instance_tag,
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}

resource "oci_core_network_security_group_security_rule" "tcp_in_security_group_rules" {
  count                     = length(local.tcp_in_security_group_rules)
  network_security_group_id = oci_core_network_security_group.network_security_group.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source                    = split(" ", local.tcp_in_security_group_rules[count.index])[4]
  tcp_options {
    destination_port_range {
      max = split(" ", local.tcp_in_security_group_rules[count.index])[3]
      min = split(" ", local.tcp_in_security_group_rules[count.index])[2]
    }
  }
}

resource "oci_core_network_security_group_security_rule" "udp_in_security_group_rules" {
  count                     = length(local.udp_in_security_group_rules)
  network_security_group_id = oci_core_network_security_group.network_security_group.id
  direction                 = "INGRESS"
  protocol                  = "17"
  source                    = split(" ", local.udp_in_security_group_rules[count.index])[4]
  udp_options {
    destination_port_range {
      max = split(" ", local.udp_in_security_group_rules[count.index])[3]
      min = split(" ", local.udp_in_security_group_rules[count.index])[2]
    }
  }
}

resource "oci_core_network_security_group_security_rule" "tcp_out_security_group_rules" {
  count                     = length(local.tcp_out_security_group_rules)
  network_security_group_id = oci_core_network_security_group.network_security_group.id
  direction                 = "EGRESS"
  protocol                  = "6"
  destination               = split(" ", local.tcp_out_security_group_rules[count.index])[4]
  tcp_options {
    destination_port_range {
      max = split(" ", local.tcp_out_security_group_rules[count.index])[3]
      min = split(" ", local.tcp_out_security_group_rules[count.index])[2]
    }
  }
}

resource "oci_core_network_security_group_security_rule" "udp_out_security_group_rules" {
  count                     = length(local.udp_out_security_group_rules)
  network_security_group_id = oci_core_network_security_group.network_security_group.id
  direction                 = "EGRESS"
  protocol                  = "17"
  destination               = split(" ", local.udp_out_security_group_rules[count.index])[4]
  udp_options {
    destination_port_range {
      max = split(" ", local.udp_out_security_group_rules[count.index])[3]
      min = split(" ", local.udp_out_security_group_rules[count.index])[2]
    }
  }
}

resource "oci_core_subnet" "vm-subnet" {

  count          = local.external-network != null ? 0 : 1
  cidr_block     = var.cidr
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vm-vcn[0].id
  route_table_id = oci_core_route_table.vm-igw-rt[0].id

  freeform_tags = {
    Name               = var.instance_name,
    instance_tag       = var.instance_tag,
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}


resource "oci_core_internet_gateway" "vm-igw" {
  count          = local.external-network != null ? 0 : 1
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vm-vcn[0].id
  enabled        = true
}


resource "oci_core_route_table" "vm-igw-rt" {
  count          = local.external-network != null ? 0 : 1
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vm-vcn[0].id
  route_rules {
    network_entity_id = oci_core_internet_gateway.vm-igw[0].id
    destination       = "0.0.0.0/0"
  }

  freeform_tags = {
    Name               = var.instance_name,
    instance_tag       = var.instance_tag,
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}