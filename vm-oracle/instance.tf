locals {
  dns_ipv4         = oci_core_instance.vm-oracle.public_ip
  external-volumes = tolist(jsondecode(var.external_volumes))
  external-network = length(tolist(jsondecode(var.external_networks))) != 0 ? tolist(jsondecode(var.external_networks))[0] : null
  device_index_mapping = {
    "0"  = "b", "1" = "c", "2" = "d", "3" = "e", "4" = "f",
    "5"  = "aa", "6" = "ab", "7" = "ac", "8" = "ad", "9" = "ae",
    "10" = "af", "11" = "ba", "12" = "bb", "13" = "bc", "14" = "bd",
    "15" = "be", "16" = "bf", "17" = "ca", "18" = "cb", "19" = "cc",
    "20" = "cd", "21" = "ce", "22" = "cf", "23" = "da", "24" = "db",
    "25" = "dc", "26" = "dd", "27" = "de", "28" = "df", "29" = "ea",
    "30" = "eb", "31" = "ec", "32" = "ed"
  }
}

data "oci_core_images" "images" {
  compartment_id = var.image_compartment_id
  display_name   = var.image_name
  sort_by        = "TIMECREATED"
  sort_order     = "DESC"
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "random_password" "password" {
  length           = 24
  special          = true
  override_special = "&"
}

resource "oci_core_instance" "vm-oracle" {
  availability_domain = var.availability_domain
  compartment_id      = var.compartment_id

  shape = var.shape
  shape_config {
    memory_in_gbs = var.memory
    ocpus         = var.cpu
  }

  create_vnic_details {
    subnet_id        = local.external-network != null ? local.external-network.subnet_id.value : oci_core_subnet.vm-subnet[0].id
    assign_public_ip = true
    nsg_ids          = [oci_core_network_security_group.network_security_group.id]

  }

  metadata = {
    "ssh_authorized_keys" = tls_private_key.private_key.public_key_openssh
  }
  source_details {
    source_id               = data.oci_core_images.images.images[0].id
    source_type             = "image"
    boot_volume_size_in_gbs = max(var.boot_volume_size, var.min_os_disk_size)
  }
  freeform_tags = {
    Name               = var.instance_name,
    instance_tag       = var.instance_tag,
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}

resource "oci_core_volume_attachment" "external-volumes-attach" {
  count           = length(local.external-volumes)
  attachment_type = "paravirtualized"
  instance_id     = oci_core_instance.vm-oracle.id
  device          = "/dev/oracleoci/oraclevd${local.device_index_mapping[local.external-volumes[count.index].index.value]}"
  volume_id       = local.external-volumes[count.index].id.value
}

resource "null_resource" "wait_for_instance" {
  count      = var.shape != null && can(regex("^BM", var.shape)) ? 1 : 0
  depends_on = [oci_core_instance.vm-oracle]

  provisioner "local-exec" {
    command = <<-EOT
      counter=0
      while [ $counter -lt 90 ]; do
        if nc -zvw3 ${oci_core_instance.vm-oracle.public_ip} 22; then
          echo 'Port 22 is accessible now'
          exit 0
        fi
        counter=$((counter + 1))
        sleep 10
      done
      echo 'Port 22 is not accessible after 900 seconds'
      exit 1
    EOT
  }
}

