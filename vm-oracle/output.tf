output "vm_id" {
  value = oci_core_instance.vm-oracle.id
}

output "id" {
  value = oci_core_instance.vm-oracle.id
}

output "ip" {
  value = oci_core_instance.vm-oracle.public_ip
}

output "local_ip" {
  value = local.external-network != null ? oci_core_instance.vm-oracle.private_ip : null
}

output "private_key" {
  value     = tls_private_key.private_key.private_key_pem
  sensitive = true
}

output "public_key" {
  value = tls_private_key.private_key.public_key_openssh
}

output "instance_user" {
  value = var.user_name
}

output "security_group_id" {
  value = oci_core_network_security_group.network_security_group.id
}

output "min_os_disk_size" {
  value = var.min_os_disk_size
}

output "flavor_name" {
  value = var.shape
}

output "terraform_script_version" {
  value = 1.2
}

